//
// Created by Mereslann on 9/09/2019.
//

#include "Inventory.h"
#include <iostream>
using namespace std;


//Adds a node to the start of the List
void Inventory::Insert(int newdata) {
    auto* newnode = (struct Node*) malloc(sizeof(struct Node));
    newnode->data  = newdata;\
    // node is always at start so prev is always null
    newnode->prev = NULL;
    newnode->next = head;
    if(head !=  NULL)
        head->prev = newnode ;
    head = newnode;
}
void Inventory::Display() {
    struct Node* ptr;
    ptr = head;
    while(ptr != NULL) {
        cout<< ptr->data <<" ";
        ptr = ptr->next;
    }
}

void Inventory::Delete(Node** head_ref, Node* del) {
    /* base case */
    if (*head_ref == NULL || del == NULL)
        return;

    /* If node to be deleted is head node */
    if (*head_ref == del)
        *head_ref = del->next;

    /* Change next only if node to be
    deleted is NOT the last node */
    if (del->next != NULL)
        del->next->prev = del->prev;

    /* Change prev only if node to be
    deleted is NOT the first node */
    if (del->prev != NULL)
        del->prev->next = del->next;

    /* Finally, free the memory occupied by del*/
    free(del);
}

Node* Inventory::GetNode(int data){
    struct Node* ptr;
    ptr = head;
    while(ptr != NULL) {
        if(ptr->data == data){
            return ptr;
        }
        ptr = ptr->next;
    }
}



Inventory::Inventory() {
    Inventory::head = NULL;
};

Inventory::~Inventory() = default;