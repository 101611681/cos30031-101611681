//
// Created by Mereslann on 9/09/2019.
//

#ifndef TASK_9_INVENTORY_H
#define TASK_9_INVENTORY_H

struct Node {
    int data;
    struct Node *prev;
    struct Node *next;
};

class Inventory {
public:
    Inventory();
    virtual ~Inventory();

    void Insert(int newdata);
    void Display();
    void Delete(Node** head_ref, Node* del);
    Node* GetNode(int data);

    Node* head;

};


#endif //TASK_9_INVENTORY_H
