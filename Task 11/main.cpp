#include <iostream>
#include <fstream>

using namespace std;

struct example {
    int myint;
    char mychar;
    float myfloat;
};

void printstruct(example* e){
    cout<< e->myint<< " " << e->mychar<< " " << e->myfloat << endl;
    cout<< "print data" <<endl;
};

void writetofile(example* e){
    ofstream myfile;
    myfile.open("myfile.txt");
    myfile.write((char*)e, sizeof(example));
    myfile.close();
}

void readfromfile(example* e){
    ifstream myfile;
    myfile.open("myfile.bin");
    myfile.seekg(0);
    myfile.read((char*)e, sizeof(example));
    myfile.close();
    cout<< "read file" <<endl;
}

int main() {
    example* mystruct = new example;

//    mystruct->myfloat = 0.1;
//    mystruct->mychar = 'c';
//    mystruct->myint = 12;
    writetofile(mystruct);
//    readfromfile(mystruct);
//    printstruct(mystruct);
    return 0;
}