#include <iostream>
#include <fstream>
#include <string>

using namespace std;


void splitstrings(string s, string delimiter){
    size_t pos = 0;
    string token;
    while((pos = s.find(delimiter)) != string::npos){
        token = s.substr(0,pos);
        cout <<token <<endl;
        s.erase(0,pos + delimiter.length());
    }
    cout << s << endl;
}

void readfromfile(){
    string line;
    ifstream myfile ("test.txt");
    if(myfile.is_open())
    {
        while (getline(myfile, line)){
            if(line.empty()){}
            else if(line.front() == '#'){}
            else{
                splitstrings(line, ":");
//                cout << line << '\n';
            }
        }
        myfile.close();
    }
    else cout << "Unable to open file";
}

int main() {
    readfromfile();
    return 0;
}