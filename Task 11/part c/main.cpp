#include <iostream>
#include <fstream>

#include <nlohmann/json.hpp>


using json = nlohmann::json;
using namespace std;


int main() {
    ifstream myfile("test3.json", ifstream::in);
    json j;

    if(myfile.is_open()){
        myfile >> j;
        myfile.close();
    }

    cout << j;

    return 0;
}