//
// Created by Mereslann on 3/09/2019.
//

#include <iterator>
#include "Gameplay.h"

std::vector<std::string> Gameplay::Split(const std::string& subject)
{
    std::istringstream ss{subject};
    using StrIt = std::istream_iterator<std::string>;
    std::vector<std::string> container{StrIt{ss}, StrIt{}};
    return container;
}

void Gameplay::addEdgeFromFile(int src, vector<string> links) {
    int dest;
    for(auto const& value: links){
        stringstream geek(value);
        geek >> dest;
        edges->push_back({src, dest});
    }
}

Location Gameplay::getLocation(string s, string delimiter){
    vector<string> splitList;
    size_t pos = 0;
    string token;
    int count = 0;
    Location l;
    while((pos = s.find(delimiter)) != string::npos){
        token = s.substr(0,pos);
//        cout <<token <<endl;
        splitList.push_back(token);
        s.erase(0,pos + delimiter.length());
        if(count == 0){
            stringstream geek(token);
            geek >> l.id;
        } else if (count == 1){
            addEdgeFromFile(l.id, Split(token));
        } else if (count == 2){
            l.command = Split(token);
        } else if (count == 3){
            l.name = token;
        }
        count++;
    }
//    cout << s << endl;
    splitList.push_back(s);
    l.desc = s;
    return l;
}




void Gameplay::readfromfile(vector<Location>* l, string * filename){
    string line;
    ifstream myfile (*filename);
    if(myfile.is_open())
    {
        while (getline(myfile, line)){
            if(line.empty()){}
            else if(line.front() == '#'){}
            else{
                l->push_back(getLocation(line, ":"));
            }
        }
        myfile.close();
    }
    else cout << "Unable to open file";
}

Gameplay::Gameplay(string * filename) : GameState(std::string("Gameplay")) {

    edges = new vector<Edge>;

    player = new class Player();

    readfromfile(&locations, filename);
    player->location = locations[0];

    // construct graph
    graph = new Graph(*edges, locations);

    // print adjacency list representation of graph
}

Gameplay::~Gameplay() = default;

std::string Gameplay::Input(Zorkish *game) {
//    cout << "phase 1 commands quit and hiscore:> ";
    string dir;
    getline(cin, dir);
    return dir;
}
void Gameplay::Update(Zorkish *game, std::string input) {
    vector<string> modinput = Split(input);
    if(modinput[0] == "quit"){
        game->SetState(game->MainMenu);
    }
    else if(modinput[0] == "hiscore"){
        game->SetState(game->NewHighScore);
    }
    else if(modinput[0] == "show"){
        graph->printAdjLocations(*graph, player->location.id);
    }
    else if(modinput[0] == "go"){
        if(modinput.size() > 1){
            player->location = graph->getNewLocation(*graph, player->location.id, modinput[1]);
        }
    }
}
void Gameplay::Render(Zorkish *game) {
    std::cout << "---------------------------------------------------------" <<std::endl;
    std::cout << "Zorkish : : Gameplay" <<std::endl;
    std::cout << "---------------------------------------------------------" <<std::endl;
    std::cout << player->location.name <<std::endl;
}

