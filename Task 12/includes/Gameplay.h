//
// Created by Mereslann on 3/09/2019.
//

#ifndef TASK_9_GAMEPLAY_H
#define TASK_9_GAMEPLAY_H

#include <iostream>
#include "GameState.h"
#include "Zorkish.h"
#include "Player.h"
#include "Graph.h"
#include "Location.h"
#include <fstream>
#include <sstream>
#include <string>


class Zorkish;


class Gameplay : public GameState {
public:
    Gameplay(string * filename);
    virtual ~Gameplay();

    virtual std::string Input(Zorkish * game);
    virtual void Render(Zorkish * game);
    virtual void Update(Zorkish * game, std::string input);

    void addEdgeFromFile(int src, vector<string> dest);

    void readfromfile(vector<Location>* l, string * filename);
    Location getLocation(string s, string delimiter);
    std::vector<std::string> Split(const std::string& subject);





private:
    Graph * graph;
    Player * player;
    vector<Edge> * edges;
    vector<Location> locations;

};

#endif //TASK_9_GAMEPLAY_H
