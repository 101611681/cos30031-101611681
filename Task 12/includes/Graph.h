//
// Created by Mereslann on 1/10/2019.
//

#ifndef TASK_12_GRAPH_H
#define TASK_12_GRAPH_H

#include <iostream>
#include <vector>
#include <map>


#include "location.h"

using namespace std;

struct Edge {
    int dest;
    int src;
//    Location src, dest;
};

class Graph {
public:
    std::map<int , vector<int>> adjList;
    Graph(const vector<Edge> &edges, const vector<Location> &locations);

    virtual ~Graph();

    void printAllLocations(Graph const& location);
    void printAdjLocations(Graph const& location, int id);
    Location getNewLocation(Graph const& location, int src, string command);

    vector<Location> locations;
};


#endif //TASK_12_GRAPH_H
