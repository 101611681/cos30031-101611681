//
// Created by Mereslann on 1/10/2019.
//

#ifndef TASK_12_ITEM_H
#define TASK_12_ITEM_H

#include <iostream>
#include <string>

class Item {
public:
    Item();
    virtual ~Item();
    std::string name;
    void Display();
};


#endif //TASK_12_ITEM_H
