//
// Created by Mereslann on 17/10/2019.
//

#include "CommandLook.h"

CommandLook::CommandLook(World *world) : Command(world) {
    syntax = "(command) ... or (command) at ...";
}

string CommandLook::Execute(vector <string> args) {


    if(args.size() > 1){
        if(args[1] == "at"){
            return world->player->location.ground.DisplayItem(args[2]);
        }
    }
    else {
        world->graph->printAdjLocations(world->player->location.id);
        world->player->location.ground.Display();
        return "";
    }
    return syntax;
}
