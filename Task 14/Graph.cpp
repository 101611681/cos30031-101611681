//
// Created by Mereslann on 1/10/2019.
//

#include "Graph.h"

Graph::Graph(vector<Edge> const &edges, vector<Location> const &loc) {
// resize the vector to N elements of type vector<int>

    locations = loc;

    // add edges to the directed graph
    for (auto &edge: edges)
    {
        // insert at the end
        adjList[edge.src].push_back(edge.dest);

        // Uncomment below line for undirected graph
         adjList[edge.dest].push_back(edge.src);
    }
}


void Graph::printAllLocations() {
    for (auto const& x : adjList)
    {
        cout << "Availiable Locations from " << locations[x.first].name  // string (key)
             << " : \n";
        for (vector<int>::size_type i = 0; i < x.second.size(); i++) {
            cout << locations[x.second.at(i)].name << ": ";
            for(auto const& value: locations[x.second.at(i)].command){
                cout << value << ' ';
            }
            cout << "\n";
        }

        cout <<endl ;
    }
}

void Graph::printAdjLocations(int id){
    for (auto const& x : adjList)
    {
        if(x.first == id){
            cout << "Availiable Locations from " << locations[x.first].name  // string (key)
                 << " : \n";
            for (vector<int>::size_type i = 0; i < x.second.size(); i++) {
                cout << locations[x.second.at(i)].name << ": ";
                for(auto const& value: locations[x.second.at(i)].command){
                        cout << value << ' ';
                }
                cout << "\n";
            }

            cout <<endl ;
        }
    }
}

Location Graph::getNewLocation(int src, string command) {
    for (auto const& x : adjList)
    {
        if(x.first == src){
            for (vector<int>::size_type i = 0; i < x.second.size(); i++) {
                for(auto const& value: locations[x.second.at(i)].command){
                    if(command == value){
                        return locations[x.second.at(i)];
                    }
                }
            }
        }
    }
}



Graph::~Graph() = default;



