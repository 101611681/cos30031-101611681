//
// Created by Mereslann on 16/10/2019.
//

#include "World.h"

World::World(string *filename) {
    edges = new vector<Edge>;

    player = new class Player();

    // get locations and items from file
    readfromfile(&this->locations, &this->items, filename);
    // adds all items to appropriate locations
    for(auto const& item: this->items){
        locations[item.first].ground.Insert(item.second);
    }
    // set player location
    player->location = locations[0];

    // construct graph
    graph = new Graph(*edges, locations);
}

vector<string>  World::Split(const string& subject)
{
    std::istringstream ss{subject};
    using StrIt = std::istream_iterator<std::string>;
    std::vector<std::string> container{StrIt{ss}, StrIt{}};
    return container;
}

void World::addEdgeFromFile(int src, vector<string> links) {
    int dest;
    for(auto const& value: links){
        stringstream geek(value);
        geek >> dest;
        edges->push_back({src, dest});
    }
}

Location World::getLocation(string s, string delimiter){
    vector<string> splitList;
    size_t pos = 0;
    string token;
    int count = 0;
    Location l;
    while((pos = s.find(delimiter)) != string::npos){
        token = s.substr(0,pos);
//        cout <<token <<endl;
        splitList.push_back(token);
        s.erase(0,pos + delimiter.length());
        if(count == 1){
            stringstream geek(token);
            geek >> l.id;
        } else if (count == 2){
            addEdgeFromFile(l.id, Split(token));
        } else if (count == 3){
            l.command = Split(token);
        } else if (count == 4){
            l.name = token;
        }
        count++;
    }
//    cout << s << endl;
    splitList.push_back(s);
    l.desc = s;
    return l;
}

vector<pair <int, Item*>> World::getItem(string s, string delimiter){
    vector<pair <int, Item*>> itemsInWorld;
    size_t pos = 0;
    string token;
    int count = 0;
    int locationid;
    Item * item = new Item();
    while((pos = s.find(delimiter)) != string::npos){
        token = s.substr(0,pos);
        s.erase(0,pos + delimiter.length());
        if(count == 1){
            stringstream geek(token);
            geek >> item->id;
        } else if (count == 2){
            item->name = token;
        }
        else if (count == 3){
            item->desc = token;
        }
        count++;
    }
//    item.desc = s;
    for(auto const& value: Split(s)){
        stringstream geek(value);
        geek >> locationid;
        itemsInWorld.emplace_back(locationid, item);
    }
    return itemsInWorld;
}

void World::readfromfile(vector<Location>* locations, multimap<int, Item*> * items, string * filename){
    string line;
    ifstream myfile (*filename);
    if(myfile.is_open())
    {
        while (getline(myfile, line)){
            if(line.empty()){}
            else if(line.front() == '#'){}
            else if (line.front() == 'L'){
                locations->push_back(getLocation(line, ":"));
            }
            else if (line.front() == 'I'){
                for(auto const& value: getItem(line, ":")){
                    items->insert(value);
                }
            }
        }
        myfile.close();
    }
    else cout << "Unable to open file";
}
