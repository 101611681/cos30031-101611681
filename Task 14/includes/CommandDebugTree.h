//
// Created by Mereslann on 17/10/2019.
//

#ifndef TASK_12_COMMANDDEBUGTREE_H
#define TASK_12_COMMANDDEBUGTREE_H


#include "Command.h"

class CommandDebugTree: public Command {
public:
    CommandDebugTree(World *world);
    virtual string Execute(vector<string> args);

};



#endif //TASK_12_COMMANDDEBUGTREE_H
