//
// Created by Mereslann on 16/10/2019.
//

#ifndef TASK_12_COMMANDMOVE_H
#define TASK_12_COMMANDMOVE_H

#include "Command.h"

class CommandMove : public Command {
public:
    CommandMove(World *world);
    virtual string Execute(vector<string> args);
};


#endif //TASK_12_COMMANDMOVE_H
