//
// Created by Mereslann on 9/09/2019.
//

#ifndef TASK_9_INVENTORY_H
#define TASK_9_INVENTORY_H

#include "Item.h"

struct Node {
    Item* data;
    struct Node *prev;
    struct Node *next;
};

class Inventory {
public:
    Inventory();
    virtual ~Inventory();

    void Insert(Item* newdata);
    void Display();
    void Delete(Node** head_ref, Node* del);
    Node* GetNode(Item* data);

    Node* head;

    string DisplayItem(string item);
};


#endif //TASK_9_INVENTORY_H
