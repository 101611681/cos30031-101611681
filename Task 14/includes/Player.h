//
// Created by Mereslann on 9/09/2019.
//

#ifndef TASK_9_PLAYER_H
#define TASK_9_PLAYER_H

#include "Inventory.h"
#include "Location.h"

class Player {
public:
    Player();
    virtual ~Player();

    Inventory PlayerInventory;

    Location location;

    void Move(Location newLocation);

};


#endif //TASK_9_PLAYER_H
