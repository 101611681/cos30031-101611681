//
// Created by Mereslann on 16/10/2019.
//

#ifndef TASK_12_WORLD_H
#define TASK_12_WORLD_H

#include <iostream>
#include "GameState.h"
//#include "Zorkish.h"
#include "Player.h"
#include "Graph.h"
#include "Location.h"
#include "Item.h"
#include <fstream>
#include <sstream>
#include <string>
#include <iterator>

using namespace std;

class World {
public:
    World(string * filename);

    vector<string> Split(const std::string& subject);

    Graph * graph;
    Player * player;

private:

    vector<Edge> * edges;
    vector<Location> locations;
    multimap<int, Item*> items;

    void addEdgeFromFile(int src, vector<string> dest);

    void readfromfile(vector<Location>* locations, multimap<int, Item*> * items, string * filename);
    Location getLocation(string s, string delimiter);

    vector<pair <int, Item*>> getItem(string s, string delimiter);

};


#endif //TASK_12_WORLD_H
