//
// Created by Mereslann on 21/10/2019.
//

#include "GameObject.h"

GameObject::GameObject() {

}

GameObject::~GameObject() {

}

bool GameObject::checkComponent(string componentKey) {
    for(auto const& component: Components){
        if(component.first == componentKey){
            return true;
        }
    }
    return false;
}

