//
// Created by Mereslann on 22/10/2019.
//

#ifndef TASK_12_COMMANDOPEN_H
#define TASK_12_COMMANDOPEN_H

#include "Command.h"

class CommandOpen: public Command {
public:
    CommandOpen(World *world);
    virtual string Execute(vector<string> args);

};


#endif //TASK_12_COMMANDOPEN_H
