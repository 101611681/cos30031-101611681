//
// Created by steve on 29/10/2019.
//

#ifndef TASK_12_COMPONENT_H
#define TASK_12_COMPONENT_H


class Component {
public:
    Component();
    virtual ~Component();
};


#endif //TASK_12_COMPONENT_H
