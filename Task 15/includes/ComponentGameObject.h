//
// Created by steve on 29/10/2019.
//

#ifndef TASK_12_COMPONENTGAMEOBJECT_H
#define TASK_12_COMPONENTGAMEOBJECT_H

#include "Component.h"

class ComponentGameObject : public Component{
public:
    ComponentGameObject();
    virtual ~ComponentGameObject();
};


#endif //TASK_12_COMPONENTGAMEOBJECT_H
