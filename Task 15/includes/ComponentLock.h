//
// Created by steve on 29/10/2019.
//

#ifndef TASK_12_COMPONENTLOCK_H
#define TASK_12_COMPONENTLOCK_H

#include "Component.h"

class ComponentLock : public Component{
public:
    ComponentLock();

    void Lock();
    void Unlock();

private:
    bool _locked;
};


#endif //TASK_12_COMPONENTLOCK_H
