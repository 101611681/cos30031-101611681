//
// Created by Mereslann on 1/10/2019.
//

#ifndef TASK_12_GRAPH_H
#define TASK_12_GRAPH_H

#include <iostream>
#include <vector>
#include <map>


#include "location.h"

using namespace std;

struct Edge {
    string command;
    int dest;
    int src;
//    Location src, dest;
};

class Graph {
public:
    std::map<int , map<string, int>> adjList;
    Graph(const vector<Edge> &edges, const vector<Location> &locations);

    virtual ~Graph();

    void printAllLocations();
    void printAdjLocations(int id);
    Location * getNewLocation(int src, string command);

    vector<Location> locations;
};


#endif //TASK_12_GRAPH_H
