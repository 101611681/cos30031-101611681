//
// Created by Mereslann on 17/10/2019.
//

#ifndef TASK_12_COMMANDINVENTORY_H
#define TASK_12_COMMANDINVENTORY_H


#include "Command.h"

class CommandInventory: public Command {
public:
    CommandInventory(World *world);
    virtual string Execute(vector<string> args);

};


#endif //TASK_12_COMMANDINVENTORY_H
