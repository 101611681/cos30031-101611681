//
// Created by steve on 29/10/2019.
//

#ifndef TASK_12_COMPONENTHEALTH_H
#define TASK_12_COMPONENTHEALTH_H

#include "Component.h"

class ComponentHealth : public Component{
public:
    ComponentHealth();
    virtual ~ComponentHealth();

    void Heal();
    void Damage();

private:
    int _health;
};


#endif //TASK_12_COMPONENTHEALTH_H
