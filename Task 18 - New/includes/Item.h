//
// Created by Mereslann on 1/10/2019.
//

#ifndef TASK_12_ITEM_H
#define TASK_12_ITEM_H

#include <iostream>
#include <string>

#include "GameObject.h"
#include "Inventory.h"

using namespace std;

class Item : public GameObject, public Inventory{
public:
    Item();
    virtual ~Item();
    void DisplayName();
//    void Display();
    Item* Copy();

//    Inventory inventoryComponent;
private:
};


#endif //TASK_12_ITEM_H
