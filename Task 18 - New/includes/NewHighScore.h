//
// Created by Mereslann on 3/09/2019.
//

#ifndef TASK_9_NEWHIGHSCHORE_H
#define TASK_9_NEWHIGHSCHORE_H


#include "GameState.h"
#include "Zorkish.h"


class Zorkish;


class NewHighScore : public GameState {
public:
    NewHighScore();
    virtual ~NewHighScore();

    virtual std::string Input(Zorkish * game);
    virtual void Render(Zorkish * game);
    virtual void Update(Zorkish * game, std::string input);

};


#endif //TASK_9_NEWHIGHSCHORE_H
