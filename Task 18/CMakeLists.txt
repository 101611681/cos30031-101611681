cmake_minimum_required(VERSION 3.14)
project(Task_12)

set(CMAKE_CXX_STANDARD 17)

add_executable(Task_12 main.cpp
        Zorkish.cpp
        GameState.cpp
        MainMenu.cpp
        About.cpp
        Help.cpp
        SelectAdventure.cpp
        Gameplay.cpp
        NewHighScore.cpp
        ViewHallOfFame.cpp
        Player.cpp
        Inventory.cpp
        Item.cpp
        Graph.cpp
        Location.cpp
        World.cpp
        Command.cpp
        CommandMove.cpp
        CommandManager.cpp
        CommandLook.cpp
        CommandHelp.cpp
        CommandInventory.cpp
        CommandDebugTree.cpp
        CommandAlias.cpp
        GameObject.cpp
        CommandTake.cpp CommandPut.cpp
        CommandOpen.cpp
        EntityComponent.cpp MessageComponent.cpp includes/MessageComponent.h)

include_directories(includes)
