//
// Created by Mereslann on 17/10/2019.
//

#include "CommandLook.h"

CommandLook::CommandLook(World *world) : Command(world) {
    syntax = "(command) ... ; (command) at ... ; (command) in ... ;";
}

string CommandLook::Execute(vector <string> args) {


    if(args.size() > 1){
        if( world->player->location->inventoryComponent.ItemExists(args[2])){
            if(args[1] == "at"){
                return world->player->location->inventoryComponent.DisplayItem(args[2]);
            }
            else if(args[1] == "in"){
                if(world->player->location->inventoryComponent.GetItem(args[2])->entityComponent.isLocked){
                    return "This item is locked";
                }
                world->player->location->inventoryComponent.GetItem(args[2])->inventoryComponent.Display();
                return "";
            }
        }
        else{
            return "item doesnt exist on ground";
        }
    }
    else {
        world->graph->printAdjLocations(world->player->location->id);
        world->player->location->inventoryComponent.Display();
        return "";
    }
    return syntax;
}
