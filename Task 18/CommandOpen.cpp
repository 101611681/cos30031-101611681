//
// Created by Mereslann on 22/10/2019.
//

#include "CommandOpen.h"

CommandOpen::CommandOpen(World *world) : Command(world) {

}

string CommandOpen::Execute(vector<string> args) {
    if(args.size() > 1){
        if(world->player->location->inventoryComponent.ItemExists(args[1])){
            Item * item = world->player->location->inventoryComponent.GetItem(args[1]);
            if(args.size() == 2) {
                {
                    world->player->messageComponent.Dispatch("unlock", item);
                    return "You have opened the " + item->name;
                }
            }
            if(args.size() == 4){
                if(args[2] == "with"){
                    if(world->player->location->inventoryComponent.ItemExists(args[3])){
                        Item * container = world->player->location->inventoryComponent.GetItem(args[3]);
                        container->messageComponent.Dispatch(args[0], item);
                        return "You have unlocked " + container->name + " with " + item->name;
                    }
                    else{
                        return "item to open container doesnt exist on the ground, sorry";
                    }
                }
                else{
                    return "no container specified";
                }
            }
        }
        else{
            return "Item doesnt exist on ground";
        }
    }
    return syntax;
}
