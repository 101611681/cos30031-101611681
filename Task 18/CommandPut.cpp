//
// Created by Mereslann on 22/10/2019.
//

#include "CommandPut.h"

CommandPut::CommandPut(World *world) : Command(world) {

}

string CommandPut::Execute(vector<string> args) {
    string msg;
    if(args.size() > 1){
        if(world->player->inventoryComponent.ItemExists(args[1])){
            world->player->inventoryComponent.MoveItem(&world->player->location->inventoryComponent, world->player->inventoryComponent.GetItem(args[1]));
            return "You have placed " + world->player->location->inventoryComponent.GetItem(args[1])->name + " on the ground";
        }

        if(args.size() > 2){
            if(args[2] == "in"){
                if(world->player->location->inventoryComponent.ItemExists(args[3])){
                    msg += "You have placed " + world->player->location->inventoryComponent.GetItem(args[1])->name + " into " + world->player->location->inventoryComponent.GetItem(args[3])->name;
                    world->player->location->inventoryComponent.MoveItem(&world->player->location->inventoryComponent.GetItem(args[3])->inventoryComponent, world->player->location->inventoryComponent.GetItem(args[1]));
                }
                else{
                    msg += "Container or item doesnt exist, sorry";
                }
            }
        }
        else{
            msg += "There is no item in your inventory";
        }
    }
    return msg;
}
