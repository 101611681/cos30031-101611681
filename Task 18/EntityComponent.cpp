//
// Created by Mereslann on 27/10/2019.
//

#include "EntityComponent.h"

EntityComponent::EntityComponent() {
    health = 1;
    canAttack = true;
    canPickup = true;
    isLocked = false;
    onFire = false;
    flammable = true;
}

EntityComponent::~EntityComponent() {

}

void EntityComponent::Damage() {
    health -= 1;
}

void EntityComponent::Ignite() {
if(flammable){
    onFire = true;
}
}

void EntityComponent::Lock() {
isLocked = true;
}

void EntityComponent::Unlock() {
isLocked = false;
}

