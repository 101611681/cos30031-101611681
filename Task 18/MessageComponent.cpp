//
// Created by Mereslann on 28/10/2019.
//

#include "MessageComponent.h"
#include "GameObject.h"

MessageComponent::MessageComponent() {

}

MessageComponent::~MessageComponent() {

}

void MessageComponent::Dispatch(string command, GameObject* gameObject) {
    gameObject->messageComponent.Recieve(command, gameObject);
}

void MessageComponent::Recieve(string command, GameObject* gameObject) {
if(command == "unlock" || command == "open"){
    gameObject->entityComponent.Unlock();
}
    if(command == "lock"){
        gameObject->entityComponent.Unlock();
    }
if(command == "ignite"){
    gameObject->entityComponent.Ignite();
}
if(command == "damage"){
    gameObject->entityComponent.Damage();
}
if(command == "info"){
    gameObject->DisplayName();
    gameObject->DisplayDescription();
}
}

void MessageComponent::Broadcast(string command, vector<GameObject *> gameObjects) {
for(auto const& gameObject: gameObjects){
    gameObject->messageComponent.Recieve(command, gameObject);
}
}




