//
// Created by Mereslann on 17/10/2019.
//

#ifndef TASK_12_COMMANDHELP_H
#define TASK_12_COMMANDHELP_H

#include "Command.h"

class CommandHelp: public Command {
public:
    CommandHelp( World* world, map<string , Command*>* commands);

    virtual string Execute(vector<string> args);
private:
    map<string , Command*>* commands;
};
#endif //TASK_12_COMMANDHELP_H
