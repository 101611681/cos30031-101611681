//
// Created by Mereslann on 17/10/2019.
//

#ifndef TASK_12_COMMANDLOOK_H
#define TASK_12_COMMANDLOOK_H

#include "Command.h"

class CommandLook: public Command {
public:
    CommandLook(World *world);
    virtual string Execute(vector<string> args);

};


#endif //TASK_12_COMMANDLOOK_H
