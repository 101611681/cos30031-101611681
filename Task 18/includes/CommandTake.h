//
// Created by Mereslann on 22/10/2019.
//

#ifndef TASK_12_COMMANDTAKE_H
#define TASK_12_COMMANDTAKE_H


#include "Command.h"

class CommandTake: public Command {
public:
    CommandTake(World *world);
    virtual string Execute(vector<string> args);

};

#endif //TASK_12_COMMANDTAKE_H
