//
// Created by Mereslann on 27/10/2019.
//

#ifndef TASK_12_ENTITYCOMPONENT_H
#define TASK_12_ENTITYCOMPONENT_H


class EntityComponent {
public:
    EntityComponent();
    ~EntityComponent();

    void Damage();
    void Ignite();
    void Lock();
    void Unlock();

    bool canPickup;
    bool canAttack;
    bool flammable;

    int health;
    bool onFire;
    bool isLocked;
};


#endif //TASK_12_ENTITYCOMPONENT_H
