//
// Created by Mereslann on 21/10/2019.
//

#ifndef TASK_12_GAMEOBJECT_H
#define TASK_12_GAMEOBJECT_H


#include <iostream>
#include <string>
#include "EntityComponent.h"
#include "MessageComponent.h"

using namespace std;

class GameObject{
public:
    GameObject();
    virtual ~GameObject();
    virtual void DisplayName() = 0;
    virtual void DisplayDescription() = 0;

    int id;
    string name;
    string desc;
    EntityComponent entityComponent;
    MessageComponent messageComponent;
};


#endif //TASK_12_GAMEOBJECT_H
