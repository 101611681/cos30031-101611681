//
// Created by Mereslann on 1/10/2019.
//

#ifndef TASK_12_LOCATION_H
#define TASK_12_LOCATION_H

#include <iostream>
#include <string>
#include <vector>
#include "Inventory.h"

using namespace std;

class Location{
public:
    int id;
    Location(int id, string name, string desc);
    Location();
    virtual ~Location();

    string name;
    string desc;
    vector<string> command;
    Inventory inventoryComponent;
};


#endif //TASK_12_LOCATION_H
