//
// Created by Mereslann on 28/10/2019.
//

#ifndef TASK_12_MESSAGECOMPONENT_H
#define TASK_12_MESSAGECOMPONENT_H

#include "string"
#include "vector"

using namespace std;

class GameObject;

class MessageComponent {
public:
    MessageComponent();
    virtual ~MessageComponent();

    void Dispatch(string command, GameObject* gameObject);
    void Broadcast(string command, vector<GameObject*> gameObjects);
    void Recieve(string command, GameObject* gameObject);
};


#endif //TASK_12_MESSAGECOMPONENT_H
