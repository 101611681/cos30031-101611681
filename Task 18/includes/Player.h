//
// Created by Mereslann on 9/09/2019.
//

#ifndef TASK_9_PLAYER_H
#define TASK_9_PLAYER_H

#include "Inventory.h"
#include "Location.h"

class Player : public GameObject{
public:
    Player();
    virtual ~Player();

    Location * location;

    void Move(Location * newLocation);
    void DisplayName();
    void DisplayDescription();

    Inventory inventoryComponent;

};


#endif //TASK_9_PLAYER_H
