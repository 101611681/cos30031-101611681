#include <iostream>
#include <string>
#include <random>

using namespace std;

int print(int input_string){
    cout << input_string;
}


int simple_maths(int x, int y){
    int sum = x + y;
    return sum;

}

void simple_print(int x, int y){
    print(x);
    print(y);
}

void simple_pointer(){
    int value;
    int * mypointer;

    mypointer = &value;
    *mypointer = 10;

    print(value);
}

void simple_loop(){
    for(int i = 0; i < 20; i++){
        if(i % 2 != 0){
            cout << i << ' ';
        }
    }
}

void simple_array(){
    srand(121312);
    int myarray[5];
    for (int i = 0; i < 4; ++i){
        myarray[i] = rand();
        cout << myarray[i] << ' ';
    }
}

void simple_split(string str){
    string word = "";
    for (auto x : str)
    {
        if (x == ' ')
        {
            cout << word << endl;
            word = "";
        }
        else
        {
            word = word + x;
        }
    }
    cout << word << endl;
}

class SimpleClass {
    private:
        int private_num = 10;
    public:
        int public_num = 20;

        int simple_maths(int y){
            int sum = private_num + y;
            return sum;
        }
};

//  enumerations
enum color {
    red,
    green,
    blue
};

int main() {
    string test_string = "Hello, World!";
//    simple_print(1, 2);
//    print(simple_maths(1, 2));
//    simple_pointer();
//    simple_loop();
//    simple_array();
//    string str = "this sentence has spaces";
//    simple_split(str);
    SimpleClass myclass;
    myclass.public_num = 30;
//    print(myclass.public_num);
//    print(myclass.private_num);
//    myclass.simple_maths(myclass.public_num);
    return 0;
}

//  single like comment

/*
 *  Multi-line comment
 */