#include <iostream>
#include <string>

using namespace std;

enum direction {
    NORTH,
    EAST,
    SOUTH,
    WEST
};

class Player {
private:
    void MoveUp(){
        y -= 1;
    }
    void MoveDown(){
        y += 1;
    }
    void MoveLeft(){
        x -= 1;
    }
    void MoveRight(){
        x +=1;
    }
public:
    int x = 1;
    int y = 6;
    bool dead = false;

    void Move(direction d){
        switch(d){
            case NORTH: MoveUp(); break;
            case EAST: MoveRight(); break;
            case SOUTH: MoveDown(); break;
            case WEST: MoveLeft(); break;
        }
    }
    void Die(){
        dead = true;
    }

};

class World {
public:
    Player myPlayer;
    string availableDirections = ("Available directions: ");
    string North = ("N ");
    string South = ("S ");
    string East = ("E ");
    string West = ("W ");

    bool gameover = false;
    bool win = false;

    char map[8][8] = {{'#', '#', '#', '#', '#', '#', '#', '#'},
                      {'#', 'G', ' ', 'D', '#', 'D', ' ', '#'},
                      {'#', ' ', ' ', ' ', '#', ' ', ' ', '#'},
                      {'#', '#', '#', ' ', '#', ' ', 'D', '#'},
                      {'#', ' ', ' ', ' ', '#', ' ', ' ', '#'},
                      {'#', ' ', '#', '#', '#', '#', ' ', '#'},
                      {'#', 'S', ' ', ' ', ' ', ' ', ' ', '#'},
                      {'#', '#', '#', '#', '#', '#', '#', '#'}};

    void PrintMap() {
        if(map[myPlayer.y][myPlayer.x] != 'G' && map[myPlayer.y][myPlayer.x] != 'D'){
            map[myPlayer.y][myPlayer.x] = 'S';
        }

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                cout << map[i][j];
            }
            cout << endl;
        }
    }

    void ResetMap() {
        map[myPlayer.y][myPlayer.x] = ' ';
    }

    bool CheckMove(direction d){
        if(d == NORTH){
            if(map[myPlayer.y - 1][myPlayer.x] != '#') {
                return true;
            }
        } else if(d == EAST) {
            if(map[myPlayer.y][myPlayer.x + 1] != '#') {
                return true;
            }
        } else if(d == SOUTH){
            if(map[myPlayer.y + 1][myPlayer.x] != '#') {
                return true;
            }
        } else if(d == WEST){

            if(map[myPlayer.y][myPlayer.x - 1] != '#') {
                return true;
            }
        };
        return false;
    }

    void availableMoves(){
        if(map[myPlayer.y - 1][myPlayer.x] != '#') {
            availableDirections += North;
        }
        if(map[myPlayer.y][myPlayer.x + 1] != '#')
        {
            availableDirections += East;
        }
        if(map[myPlayer.y + 1][myPlayer.x] != '#') {
            availableDirections += South;
        }
        if(map[myPlayer.y][myPlayer.x - 1] != '#') {
            availableDirections += West;
        }
    }
    void PrintMove(){
        availableMoves();
        cout << availableDirections <<endl;
        availableDirections = ("Available directions: ");
    }

    void CheckState(){
        if(map[myPlayer.y][myPlayer.x] == 'G'){
            gameover = true;
            win = true;
        }
        if(map[myPlayer.y][myPlayer.x] == 'D'){
            myPlayer.Die();
            gameover = true;
        }
    }
};


void Update(World *myWorld) {
    cout << "Choose your desired direction(n, e, s, w) or press q to quit: ";
    char dir;
    cin >> dir;
    dir = toupper(dir);

    direction move;


    switch(dir){
        case 'N': move = NORTH; break;
        case 'E': move = EAST; break;
        case 'S': move = SOUTH; break;
        case 'W': move = WEST; break;
        case 'Q': myWorld->gameover = true; return;
    }

    if (myWorld->CheckMove(move)) {
        myWorld->myPlayer.Move(move);
    }
    myWorld->CheckState();

}

void Render(World *myWorld){
    if (myWorld->gameover) {
        if (myWorld->myPlayer.dead) {
            cout << "Oh no you died" << endl;
        } else if (myWorld->win){
            cout << "Congrats you win" << endl;
        };
    } else{
        myWorld->PrintMove();
        myWorld->PrintMap();
        myWorld->ResetMap();
    }
}

int main() {
    World myWorld;
    World *worldPointer = &myWorld;
    Render(worldPointer);
//    worldPointer->PrintMap();
    while (!worldPointer->gameover){
        Update(worldPointer);
        Render(worldPointer);
    }
    return 0;
}
