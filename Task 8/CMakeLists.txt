cmake_minimum_required(VERSION 3.14)
project(Task_8)

set(CMAKE_CXX_STANDARD 14)

include_directories(.)

add_executable(Task_8
        task08_sample1.cpp)
