//
// Created by Mereslann on 3/09/2019.
//

#include <iostream>
#include "Gameplay.h"

Gameplay::Gameplay() : GameState(std::string("Gameplay")) {

}

Gameplay::~Gameplay() {}

std::string Gameplay::Input(Zorkish *game) {
    std::cout << "phase 1 commands quit and hiscore:> ";
    std::string dir;
    std::cin >> dir;
    return dir;
}
void Gameplay::Update(Zorkish *game, std::string input) {
    if(input == "quit"){
        game->SetState(game->MainMenu);
    }
    else if(input == "hiscore"){
        game->SetState(game->NewHighScore);
    }
}
void Gameplay::Render(Zorkish *game) {
    std::cout << "---------------------------------------------------------" <<std::endl;
    std::cout << "Zorkish : : Gameplay" <<std::endl;
    std::cout << "---------------------------------------------------------" <<std::endl;
    std::cout << "There is nothing here. Come back next time" <<std::endl;
}